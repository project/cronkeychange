CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Links


INTRODUCTION
------------

Cron key change module makes possible to generate a new cron key.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------
 
 * Configure to generate the new cron key here:
    - /admin/config/system/cron


MAINTAINERS
-----------

Current maintainers:
 * Kurucz Istvan (nevergone ) - https://www.drupal.org/u/nevergone


LINKS
-----

Project page: https://www.drupal.org/project/cronkeychange
Submit bug reports, feature suggestions: https://www.drupal.org/project/issues/cronkeychange