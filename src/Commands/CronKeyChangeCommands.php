<?php

namespace Drupal\cronkeychange\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drupal 9 commands.
 *
 * For commands that are parts of modules, Drush expects to find commandfiles
 * in __MODULE__/src/Commands, and the namespace is Drupal/__MODULE__/Commands.
 *
 * In addition to a commandfile like this one, you need to
 * add a drush.services.yml in root of your module like this module does.
 */
class CronKeyChangeCommands extends DrushCommands {

  /**
   * Generate new cron key.
   *
   * @command cronkeychange
   */
  public function cronkeychange() : void {
    cronkeychange_generate_new_key();
    $this->output()->writeln(dt('New cron key generated.'));
  }

}
