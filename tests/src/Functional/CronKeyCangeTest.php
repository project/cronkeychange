<?php

namespace Drupal\Tests\cronkeychange\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the change cron key.
 *
 * @group cronkeychange
 */
class CronKeyCangeTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['cronkeychange'];

  /**
   * The user for the test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Set up a privileged user.
   */
  public function setUp(): void {
    parent::setUp();
    // Create and log in our privileged user.
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the change cron key.
   */
  public function testCronKeyChange() : void {
    $original_cron_key = \Drupal::state()->get('system.cron_key');
    $this->drupalGet('/admin/config/system/cron');
    $visible_cron_key = trim($this->getSession()->getPage()->find('xpath', '//*[@id="edit-current"]/text()[2]')->getText());
    $this::assertEquals($original_cron_key, $visible_cron_key, 'Original value show correctly.');
    $this->drupalGet('admin/config/system/cron');
    $this->submitForm([], t('Generate new key'));
    $visible_cron_key = trim($this->getSession()->getPage()->find('xpath', '//*[@id="edit-current"]/text()[2]')->getText());
    $this::assertNotSame($visible_cron_key, '', 'Generated cron key is not null.');
    $this::assertNotEquals($original_cron_key, $visible_cron_key, 'Cron key is changed.');
  }

}
