<?php

namespace Drupal\Tests\cronkeychange\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for cronkeychange.
 *
 * @group cronkeychange
 */
class GenericTest extends GenericModuleTestBase {}
